<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{

    public function savecategory(Request $request) {
        $this->validate($request, ['category_name' => 'required']);

        $category = new Category();
        $category->category_name = $request->category_name;
        $category->save();
        return back()->with('status', 'Votre catégorie '.$category->category_name.' a bien étée crée.');        
    }

    public function deletecategory($id) {
        $category = Category::find($id);
        $category->delete();

        return back()->with('status', 'La catégorie a bien étée supprimée.');        
    }


    public function editcategory($id) {
        $category = Category::find($id);
        return view('admin.editcategory')->with('category', $category);
    }

    public function updatecategory($id, request $request) {
        $category = Category::find($id);
        $category->category_name = $request->input('category_name');
        $category->update();

        return redirect('admin/categories')->with('status', 'Votre catégorie '.$category->category_name.' a bien étée modifiée.');        

    }
}
