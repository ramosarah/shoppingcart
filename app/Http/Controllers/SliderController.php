<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slider;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function saveslider(Request $request) {
        $this->validate($request, [
            'description1' => 'required',
            'description2' => 'required',
            'image' => 'image|nullable|max:1999'
        ]);

        //getting file name with extension
        $fileNameWithExt = $request->file('image')->getClientOriginalName();
        //getting file
        $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
        //getting extension
        $ext = $request->file('image')->getClientOriginalExtension();
        //file name to store
        $fileNameToStore = $fileName.'_'.time().'.'.$ext;
        //upoading image
        $path = $request->file('image')->storeAs('public/slider_images', $fileNameToStore);

        $slider = new Slider();
        $slider->description1 = $request->description1;
        $slider->description2 = $request->description2;
        $slider->image = $fileNameToStore;
        $slider->save();

        return back()->with('status', 'votre slider '.$slider->description1.' a été crée');
    }

    public function deleteslider($id) {
        $slider = Slider::find($id);
        Storage::delete("public/slider_images/$slider->image");
        $slider->delete();

        return back()->with('status', 'Le slider a été supprimé');
    }

    public function editslider($id) {
        $slider = Slider::find($id);
        return view('admin.editslider', compact('slider'));
    }

    public function updateslider($id, Request $request) {
        $slider = Slider::find($id);
        $slider->description1 = $request->description1;
        $slider->description2 = $request->description2;
        
        if($request->file('image')){
            $this->validate($request, [
                'image' => 'image|nullable|max:1999',
            ]);
              //getting file name with extension
            $fileNameWithExt = $request->file('image')->getClientOriginalName();
            //getting file
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            //getting extension
            $ext = $request->file('image')->getClientOriginalExtension();
            //file name to store
            $fileNameToStore = $fileName.'_'.time().'.'.$ext;
            //suppression de l'image existante
            Storage::delete("public/slider_images/$slider->image");
            //upoading image
            $path = $request->file('image')->storeAs('public/slider_images', $fileNameToStore);

            $slider->image = $fileNameToStore;
        }
        
        $slider->update();

        return back()->with('status', 'Votre slider a été modifié');
    }

    public function unactivateslider($id) {
        $slider = Slider::find($id);
        $slider->status = 0;

        $slider->update();
        return back();
    }

    public function activateslider($id) {
        $slider = Slider::find($id);
        $slider->status = 1;

        $slider->update();
        return back();
    }
}
