<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Client;
use App\Models\Order;


class ClientController extends Controller
{
    public function home() {
        $sliders = Slider::where('status', 1)->get();
        $products = Product::where('status', 1)->get();

        return view('client.home', compact('sliders', 'products'));
    }

    public function shop() {
        $products = Product::where('status', 1)->get();
        return view('client.shop', compact('products'));
    }

    public function cart() {
        return view('client.cart');
    }

    public function checkout() {
        if(Session::has('client')) return view('client.checkout');
        return redirect('/signin');
    }

    public function register() {
        return view('client.register');
    }

    public function createaccount(Request $request) {
        $this->validate($request, [
            'email' => 'email|required|unique:clients',
            'password' => 'required|min:4',
        ]);

        $client = new Client();
        $client->email = $request->email;
        $client->password = bcrypt($request->password);

        $client->save();

        return back()->with('status', 'Votre compte a été crée');

    }

    public function signin() {
        return view('client.signin');
    }

    public function accessaccount(Request $request) {
        $this->validate($request, [
            'email' => 'email|required',

        ]);

        $client = Client::where('email', $request->email)->first();

        if ($client) {
            if(Hash::check($request->password, $client->password)){
                Session::put('client', $client);
                return redirect('/shop');
            }
            return back()->with('error', 'Pas bon identifiants');
        }
        return back()->with('error', 'connexion échouée, vous n\'avez pas encore de compte');
    }

    public function logout() {
        Session::forget('client');
        return back();
    }

    public function addtocart($id) {
        $product = Product::find($id);

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart); //car nécessaire dans l'init du construct__ du models Cart.php
        $cart->add($product);
        Session::put('cart', $cart);
        Session::put('topCart', $cart->items);

        return back();
    }


    public function updateqty($id, Request $request) {
        $product = Product::find($id);

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart); //car nécessaire dans l'init du construct__ du models Cart.php
        $cart->updateQty($id, $request->qty); //updateQty() vient de Cart.php
        Session::put('cart', $cart);
        Session::put('topCart', $cart->items);

        return back();
    }

    public function removeitem($id) {
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $cart->removeItem($id);

        Session::put('cart', $cart);
        Session::put('topCart', $cart->items);

        return back();    
    }

    public function payer(Request $request){

        try{

            $oldCart = Session::has('cart') ? Session::get('cart') : null;
            $cart = new Cart($oldCart);
    
            $order = new Order();
            $order->names = $request->input('firstname')." ".$request->input('lastname');
            $order->address = $request->input('address');
            $order->cart = serialize($cart);

            Session::put('order', $order);

            $checkoutData = $this->checkoutData();

            $provider = new ExpressCheckout();
    
            $response = $provider->setExpressCheckout($checkoutData);
    
            return redirect($response['paypal_link']);

        }
        catch(\Exception $e){
            return redirect('/cart')->with('status', $e->getMessage());
        }

    }

    private function checkoutData(){

        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $data['items'] = [];

        foreach($cart->items as $item ){
                $itemDetails=[
                'name' => $item['product_name'],
                'price' => $item['product_price'],
                'qty' => $item['qty']
                ];

            $data['items'][] = $itemDetails;            
        }

        $checkoutData = [
            'items' => $data['items'],
            'return_url' => url('/paymentSuccess'),
            'cancel_url' => url('/cart'),
            'invoice_id' => uniqid(),
            'invoice_description' => "order description",
            'total' => Session::get('cart')->totalPrice
        ];

        return $checkoutData;
    }

    public function paymentSuccess(Request $request){ //ces Request viennent de PayPal

        try{

		    $token = $request->get('token');
        	$payerId = $request->get('PayerID');
        	$checkoutData = $this->checkoutData();

        	$provider = new ExpressCheckout();
        	$response = $provider->getExpressCheckoutDetails($token);
        	$response = $provider->doExpressCheckoutPayment($checkoutData, $token, $payerId);

            Session::get('order')->save();

            Session::forget('cart');
            Session::forget('topCart');

            return redirect('/cart')->with('status', 'Votre commande a été effectuée avec succès !! ');
        }
        catch(\Exception $e){
            return redirect('/cart')->with('status', $e->getMessage());
        }
    }
}
